/*
    Дасгал 04
       *
      * *
     * * *
    * * * *
     * * *
      * *
       *
*/

import java.util.Scanner;

public class Shape {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("n: ");
        int n = scanner.nextInt();

        for (int i=0; i<n; i++){
            for (int j=0; j<n-i; j++){
                System.out.print(" ");
            }
            for (int j=0; j<=i; j++){
                System.out.print("* ");
            }
            System.out.println();
        }

        for (int i=n; i>=0; i--){
            for (int j=0; j<n-i; j++){
                System.out.print(" ");
            }
            for (int j=0; j<=i; j++){
                System.out.print("* ");
            }
            System.out.println();
        }            
    }
}