/*
    Огноо: 2018-11-29
    Дасгал: 07
    Тайлбар:  XO тоглоомын хэн хожсоныг олох
    Оролт:
    1 0 -1
    -1 1 0
    1 -1 0
    Гаралт:
    1
*/

import java.util.Scanner;

public class Game {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);

        int[][] a = new int[3][3];

        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                a[i][j] = scanner.nextInt();
            }
        }
                
        for (int i=0; i<3; i++){
            if (a[i][0] != -1 && a[i][0] == a[i][1] && a[i][1] == a[i][2]){                
                System.out.println(a[i][0]);                
                return;
            }
        }

        for (int i=0; i<3; i++){
            if (a[0][i] != -1 && a[0][i] == a[1][i] && a[1][i] == a[2][i]){
                System.out.println(a[0][i]);                
                return;
            }
        }

        if (a[0][0] != -1 && a[0][0] == a[1][1] && a[1][1] == a[2][2]){
            System.out.println(a[0][0]);                
            return;
        }

        if (a[0][2] != -1 && a[0][2] == a[1][1] && a[1][1] == a[2][0]){
            System.out.println(a[0][2]);                
            return;
        }

        System.out.println(-1);                        
    }
}