/*
    Огноо: 2018-11-29
    Дасгал: 06
    Тайлбар:    Эхлээд 3 тоо оруулна, эхний 2 тоо матрицийн хэмжээ, гурав дахь тоо нь үржих ёстой тоо.
                Дараа нь өгөгдсөн хэмжээгээр матрицийн утгуудыг оруулна. Үр дүнд нь тухай матрицийг 
                өгөгдсөн тоогоор үржүүлээд, хэвлэнэ.
    Оролт:
    2 3 4
    1 2 3
    4 5 6
    Гаралт:
    4 8 12
    16 20 24
*/

import java.util.Scanner;

public class Matrix {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);
        
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int x = scanner.nextInt();

        int[][] a = new int[m][n];
        int[][] b = new int[m][n];

        for (int i=0; i<m; i++){
            for (int j=0; j<n; j++){
                a[i][j] = scanner.nextInt();
            }
        }

        for (int i=0; i<m; i++){
            for (int j=0; j<n; j++){
                b[i][j] = a[i][j] * x;
            }            
        }

        for (int i=0; i<m; i++){
            for (int j=0; j<n; j++){
                System.out.print(b[i][j] + " ");
            }
            System.out.println();
        }
    }
}