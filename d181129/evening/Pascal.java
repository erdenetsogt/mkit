/*
    Огноо: 2018-11-29
    Дасгал: 08 Паскалын гурвалжин    
    Оролт:
    4
    Гаралт:
    1
    1 1 
    1 2 1
    1 3 3 1
*/

import java.util.Scanner;

public class Pascal {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);
               
        int n = scanner.nextInt();        

        int[][] a = new int[n][n];

        a[0][0] = 1;
        
        for (int i=1; i<n; i++){
            for (int j=0; j<i+1; j++){
                if (j==0){
                    a[i][j] = a[i-1][j];
                }
                else {
                    a[i][j] = a[i-1][j] + a[i-1][j-1];
                }
                
            }
        }        

        for (int i=0; i<n; i++){
            for (int j=0; j<i+1; j++){
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
}