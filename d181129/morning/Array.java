import java.util.Scanner;

public class Array {
    public static void main(String []args){

        Scanner scanner = new Scanner(System.in);

        int[] a;

        int b[];

        a = new int[5];

        int[] c = {4, 5, 8, 2};

        System.out.println(c[0]); 

        System.out.println(c[2]); 

        System.out.println("------"); 

        // prints all elements
        for (int i=0; i<c.length; i++){                        
            System.out.println(c[i]); 
        }        

        System.out.println("-------");
        System.out.println(minNumber(12,45));

        // sum

        // largest

        System.out.print("n: ");
        int n = scanner.nextInt();
        
        b = new int[n];

        for (int i=0; i<n; i++){
            System.out.print("b[" + i + "]:");
            b[i] = scanner.nextInt();
        }

        System.out.println("-------");

        for (int i=0; i<n; i++){
            System.out.println(b[i]);            
        }
        
    }

    public static int minNumber(int a, int b){
        int min; 
        if (a>b){
            min = b;            
        }
        else {
            min = a;
        }
        return min;
    }
}