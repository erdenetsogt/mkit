/*
    Огноо: 2018-11-29
    Дасгал: Фибоначчийн дараалал
 */

import java.util.Scanner;

public class Fibonacci {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);
        
        int n = scanner.nextInt();
        
        int[] a = new int[n];

        if (n==1){
            a[0] = 0;
        }
        if (n==2){
            a[0] = 0;
            a[1] = 1;
        }
        else {
            a[0] = 0;
            a[1] = 1;
            for (int i = 2; i<n; i++){
                a[i] = a[i-1] + a[i-2];
            }
        }

        for (int i=0; i<n; i++){
            System.out.println(a[i]);
        }
    }
}