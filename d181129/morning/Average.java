/*
    Огноо: 2018-11-29
    Дасгал: Массивын дундаж олох
 */

import java.util.Scanner;

public class Average {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);
        
        int n = scanner.nextInt();

        System.out.println("-------");
        
        int[] a = new int[n];

        for (int i=0; i<n; i++){        
            a[i] = scanner.nextInt();
        }

        int sum = 0;
        for (int i=0; i<n; i++){        
            sum = sum + a[i];            
        }
        int avg = sum/n;

        System.out.println("-------");  
        System.out.println(avg);                                  
    }
}