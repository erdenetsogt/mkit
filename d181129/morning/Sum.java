/*
    Огноо: 2018-11-29
    Дасгал: Массивын нийлбэр
 */

import java.util.Scanner;

public class Sum {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);
        
        int n = scanner.nextInt();

        System.out.println("-------");
        
        int[] a = new int[n];

        for (int i=0; i<n; i++){        
            a[i] = scanner.nextInt();
        }

        int sum = 0;
        for (int i=0; i<n; i++){        
            sum = sum + a[i];            
        }

        System.out.println("-------");  
        System.out.println(sum);                                  
    }
}