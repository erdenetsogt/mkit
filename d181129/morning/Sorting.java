/*
    Огноо: 2018-11-29
    Дасгал: Массивыг эрэмбэл
 */

import java.util.Scanner;

public class Sorting {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);
        
        int n = scanner.nextInt();

        System.out.println("-------");
        
        int[] a = new int[n];

        for (int i=0; i<n; i++){        
            a[i] = scanner.nextInt();
        }

        
        int temp;
        for (int i=0; i<n; i++){        
            int min = a[i];
            int minIndex = i;        
            for (int j=i; j<n; j++){
                if (a[j]<min){
                    min = a[j];
                    minIndex = j;
                }
            }
            temp = a[i];
            a[i] = min;
            a[minIndex] = temp;            
        }
        
        System.out.println("-------");  

        for (int i=0; i<n; i++){        
            System.out.println(a[i]);                                  
        }        
    }
}