/*
    Огноо: 2018-11-29
    Дасгал: Массиваас хамгийн их тоог ол
 */

import java.util.Scanner;

public class Max {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);
        
        int n = scanner.nextInt();

        System.out.println("-------");
        
        int[] a = new int[n];

        for (int i=0; i<n; i++){        
            a[i] = scanner.nextInt();
        }

        int max = a[0];
        for (int i=1; i<n; i++){        
            if (a[i]>max){
                max = a[i];
            }
        }

        System.out.println("-------");  
        System.out.println(max);                                  
    }
}