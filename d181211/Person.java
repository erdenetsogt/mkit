public class Person {
    private String name;
    int age;
    static String citizen;
    int counter;

    public String getName() {
        return name;
    }

    public void setName (String name){        
        this.name = name;
    }

    public void intro() {
        System.out.println("Name: " + this.name);
        System.out.println("Age: " + this.age);
        System.out.println("Citizen: " + this.citizen);
        System.out.println("Counter: " + this.counter);
    }

    public void increment(){
        counter ++;
    }
}