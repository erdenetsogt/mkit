public class Example {
    public static void main(String []args){
        Person bold = new Person();        
        bold.setName("Bold");
        bold.age = 32;
        bold.citizen = "Mongolian";    

        Person john = new Person();
        john.setName("John Smith");
        john.age = 14;
        john.citizen = "American";

        bold.increment();
        bold.intro();

        john.increment();
        john.intro();        
                
        System.out.println(bold.getName());             
    }        
}

