public class ExceptionExample {
    public static void main(String []args){
        Integer[] a = new Integer[4];
        a[0] = 3;
        a[1] = 5;
        a[2] = 7;
        a[3] = 8;            
        
        System.out.println("-----");

        try {
            readText();
        }
        catch(Exception e){
            System.out.println("catched");
        }
        
    }  

    public static void readText() throws Exception {    
        throw new Exception();
    }  
        
}

