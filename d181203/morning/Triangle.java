/*
Гараас 0 тоо өгтөл өгсөн тооны хэмжээтэй гурвалжин зурах
 */

import java.util.Scanner;

public class Triangle {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);                
        while(true){
            int n = scanner.nextInt();
            if (n==0){
                break;
            }
            else {
                printTriangle(n);
            }
        }                        
    }
    
    public static void printTriangle(int n){        
        for (int i=0; i<n; i++){
            for (int j=0; j<i+1; j++){
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}