/*
Хамгийн их ерөнхий хуваагч
 */

import java.util.Scanner;

public class GCD {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);                
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        System.out.println(gcd(a,b));
    }
    
    public static int gcd(int a, int b){        
        int m = min(a, b);
        for (int i=m; i>1; i--){
            if ((a % i == 0) && (b % i == 0)){
                return i;
            }
        }        
        return 1;
    }

    public static int min(int a, int b){
        if (a<b) {
            return a;
        }
        else {
            return b;
        }        
    }

}

