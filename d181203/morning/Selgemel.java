/*
Сэлгэмэл
 */

import java.util.Scanner;

public class Selgemel {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);                
        String s = scanner.nextLine();
        String[] selgemel = selgemelArray(s);
        for (int i = 0; i<selgemel.length; i++) {
            System.out.println(selgemel[i]);
        }
    }
    
    public static String[] selgemelArray(String s){        
        String[] selgemel = new String[factorial(s.length())];        
        int count = 0;
        if (s.length()==1){
            selgemel[0] = s;            
        }
        else {
            for (int i=0; i<s.length(); i++){
                String ded = s.replace(String.valueOf(s.charAt(i)), "");
                String[] dedSelgemel = selgemelArray(ded);
                for(int j=0; j<dedSelgemel.length; j++){
                    selgemel[count] = s.charAt(i) + dedSelgemel[j];
                    count++;
                }                        
            }
        }        
        return selgemel;
    }

    public static int factorial(int n){
        if (n==1){
            return 1;
        }
        else {
            return n * factorial(n-1);
        }
    }
}

