import java.util.Scanner;

public class RemoveVowels {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);                
        String s = scanner.nextLine();   
        String s1 = s.replace("a","").replace("o","").replace("u","").replace("i","").replace("e","");
        String s2 = s1.replace("A","").replace("O","").replace("U","").replace("I","").replace("E","");
        System.out.println(s2);
    }
}