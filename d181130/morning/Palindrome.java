import java.util.Scanner;

public class Palindrome {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);                
        String s = scanner.nextLine().toLowerCase();

        int len = s.length();
        for (int i=0; i<=len/2; i++){
            if (s.charAt(i)!=s.charAt(len - 1 - i)){
                System.out.println(0);        
                return;
            }
        }
        
        System.out.println(1);
    }
}