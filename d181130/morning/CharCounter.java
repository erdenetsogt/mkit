import java.util.Scanner;

public class CharCounter {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);                
        String s = scanner.nextLine();   
        char c = scanner.nextLine().charAt(0);
        int counter = 0;
        
        for (int i=0; i<s.length(); i++){
            if (c == s.charAt(i)){
                counter++;
            }
        }        

        System.out.println(counter);
    }
}