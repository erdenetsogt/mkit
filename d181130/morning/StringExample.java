/*
    Огноо: 2018-11-30
    Дасгал: String        
*/

import java.util.Scanner;

public class StringExample {
    public static void main(String []args){
        String hello = "Hello World";
        String hi = new String("hi");
        System.out.println(hello);  
        System.out.println(hi);  

        System.out.println(hello.charAt(4));
        System.out.println(hello.length());
        System.out.println(hello.substring(3));
        System.out.println(hello.substring(3,7));
        System.out.println(hello.contains("ello"));
        System.out.println(hello.contains("sainu"));
        System.out.println(String.join("--", "Hello", "world", "again"));
        System.out.println(hello.equals("Hello World"));
        System.out.println(hello.equals("HelloWorld"));
        System.out.println(hello.isEmpty());
        System.out.println(hello.concat(" People"));
        System.out.println(hello.replace("l", "p"));
        System.out.println(hello.equalsIgnoreCase("hello world"));
        System.out.println(hello.split(" "));
        System.out.println(hello.indexOf("Wo"));
        System.out.println(hello.toUpperCase());
        System.out.println(hello.toLowerCase());
        System.out.println("  hello ".trim());

    }              
}