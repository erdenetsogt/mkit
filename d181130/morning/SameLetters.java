import java.util.Scanner;

public class SameLetters {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);                
        String s = scanner.nextLine();   
        char letter = scanner.nextLine().charAt(0);
                
        boolean same = false;

        for (int i=1; i<s.length(); i++){
            if (letter == s.charAt(i) && letter == s.charAt(i-1)){
                same = true;
                break;
            }
        }        

        System.out.println(same);
    }
}