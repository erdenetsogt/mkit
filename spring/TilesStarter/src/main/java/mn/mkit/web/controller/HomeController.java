package mn.mkit.web.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	
	@RequestMapping(value="/")
	public String  hello (Map<String, Object> model) {
		model.put("name", "MKIT");
		model.put("position", "HUB Innovation Center");
		return "home";
	}
		
}
