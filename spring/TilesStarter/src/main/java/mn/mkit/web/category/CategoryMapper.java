package mn.mkit.web.category;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface CategoryMapper {
	
	@Select("select * from category order by #{orderField} asc")
    List<Category> getAll(@Param("orderField") String field);
	
	@Select("select * from category where id=#{categoryId}")
    Category getOne(@Param("categoryId") Integer dasdsdfsasda);
	
	@Insert("insert into category(name,ordering) values(#{name},#{ordering})")
	void insertCategory(Category category);
	
	@Update("update category set name = #{name}, ordering = #{ordering} where id = #{id}")
	void updateCategory(Category category);
	
	@Delete("delete from category where id=#{categoryId}")
	void deleteCategory(@Param("categoryId") Integer das);
	
}
