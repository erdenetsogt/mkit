package mn.mkit.web.category;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoryController {
	
	@Autowired
	private CategoryMapper categoryMapper;

	@RequestMapping(value="/categories")
	public List<Category> list () {
		List<Category> categories = categoryMapper.getAll("ordering"); 		
		return categories;
	}
	
	@RequestMapping(value="/categories/{id}")
	public Category getoasdaslkdjlasNE (@PathVariable Integer id ) {				 
		return categoryMapper.getOne(id);		
	}
	
	@RequestMapping(value="/categories/save")
	public Boolean aslkdjlasNE (@RequestParam String name, @RequestParam Integer ordering) {
		Category category = new Category();
		category.setName(name);
		category.setOrdering(ordering);	
		categoryMapper.insertCategory(category);
		return Boolean.TRUE;
	}
	
	@RequestMapping(value="/categories/{id}/update")
	public Category getoadjlasNE (@PathVariable Integer id, @RequestParam String name, @RequestParam Integer ordering ) {				 
		Category category = categoryMapper.getOne(id);
		category.setName(name);
		category.setOrdering(ordering);
		categoryMapper.updateCategory(category);
		return category;
	}
	
	@RequestMapping(value="/categories/{id}/delete")
	public Boolean djlasNE (@PathVariable Integer id) {				 
		categoryMapper.deleteCategory(id);		
		return Boolean.TRUE;
	}
		
}
