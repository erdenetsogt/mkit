/*
    Дасгал 01
    Өгөгдсөн 3 тооны хамгийн ихийг олох
*/

import java.util.Scanner;

public class MaxNumber {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("a: ");
        int a = scanner.nextInt();
        System.out.print("b: ");
        int b = scanner.nextInt();
        System.out.print("c: ");
        int c = scanner.nextInt();

        int max;
        if (a>b) {
            max = a;
        }
        else {
            max = b;
        }

        if (c>max){
            max = c;
        }
        System.out.println("Max value is: " + max);
    }
}