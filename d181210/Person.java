public class Person {
    String name;
    int age;

    public Person(){
        System.out.println("created Person object");
    }

    public Person(String name, int age){
        this.name = name;
        this.age = age;
    }

    public void intro() {
        System.out.println("My name is " + this.name);
    }
}