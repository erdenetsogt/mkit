public class Example {
    public static void main(String []args){
        Person bold = new Person();        
        bold.name = "BOLD";
        bold.age = 25;
        bold.intro();     

        Person sarnai = new Person("Sarnai", 27);
        sarnai.intro();
    }        
}

