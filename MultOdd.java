/*
    Дасгал 02
    1-n хүртэлх тооны сондгой тоонуудын үржвэр
*/

import java.util.Scanner;

public class MultOdd {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("n: ");
        int n = scanner.nextInt();
        
        int mult = 1;
        for (int i=1; i<=n; i++){
            if (i%2==1){
                mult = mult * i;
            }
        }

        System.out.println("Mult: " + mult);
        
    }
}