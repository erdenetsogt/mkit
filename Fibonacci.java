/*
    Дасгал 03
    Эхний n ширхэг фибоначчийн дараалал
*/

import java.util.Scanner;

public class Fibonacci {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("n: ");
        int n = scanner.nextInt();
        
        if (n==1){
            System.out.println(0);    
        }
        else if (n==2){
            System.out.println(0);    
            System.out.println(1);    
        }
        else {
            int first = 0;
            int second = 1;            
            System.out.println(first);    
            System.out.println(second);    
            int current;
            for (int i=3; i<=n; i++){
                current = first + second;
                System.out.println(current);    
                first = second;
                second = current;
            }
        }
    }
}