public class Example {
    public static void main(String []args){
        Dog dog = new Dog();
        dog.intro();
        dog.eat();

        MyCalculation my = new MyCalculation();
        my.add();
        my.subtract();
        my.multiply();
        my.divide();

        BoldCalculation bold = new BoldCalculation();
        bold.add();
        bold.subtract();
        bold.multiply();
        bold.divide();
    }        
}

