public abstract class Animal {
    public void intro() {
        System.out.println("Animal Class");
    }    

    public abstract void eat();
}