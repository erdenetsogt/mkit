public interface Arithmetic {
    void add();
    void subtract();
    void multiply();
    void divide();
}