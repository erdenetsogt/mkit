/*
    Дасгал 07
    1
    2 3
    4 5 6 
    7 8 9 10
*/

import java.util.Scanner;

public class Floyd {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("n: ");
        int n = scanner.nextInt();
                
        int num = 1;        
        for (int i=0; i<n; i++){            
            for (int j=0; j<=i; j++){
                System.out.print(num + " ");        
                num = num + 1;
            }           
            System.out.println(); 
        }        
        
    }
}