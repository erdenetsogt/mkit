/*
    Дасгал 06
       1
      2 2
     3 3 3
    4 4 4 4
*/

import java.util.Scanner;

public class TriangleNumber {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("n: ");
        int n = scanner.nextInt();
                
        for (int i=0; i<n; i++){
            for (int j=1; j<n-i; j++){
                System.out.print(" ");        
            }
            for (int j=0; j<=i; j++){
                System.out.print((i+1) + " ");        
            }           
            System.out.println(); 
        }        
        
    }
}