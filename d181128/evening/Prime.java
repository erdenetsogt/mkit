/*
    Дасгал 09
    Эхний n ширхэг анхдагч тоо
*/

import java.util.Scanner;

public class Prime {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("n: ");
        int n = scanner.nextInt();
                
        int count = 0;        
        int number = 2;
        boolean isPrime = true;
        while (count < n){
            for (int i = 2; i <= Math.sqrt(number); i++){
                if (number % i == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime){
                System.out.println(number);
                count++;
            }
            isPrime = true;
            number++;
        }                
    }
}