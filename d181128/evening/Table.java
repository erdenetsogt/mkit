/*
    Дасгал 05
    n тооны хүрд
*/

import java.util.Scanner;

public class Table {
    public static void main(String []args){
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("n: ");
        int n = scanner.nextInt();
                
        for (int i=1; i<=10; i++){
            int mult = i * n;
            System.out.println(n + " * " + i + " = " + mult);            
        }        
        
    }
}